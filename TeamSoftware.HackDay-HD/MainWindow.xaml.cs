﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TeamSoftware.HackDay_HD.Events;
using TeamSoftware.HackDay_HD.Models;
using TeamSoftware.HackDay_HD.Repository;
using TeamSoftware.HackDay_HD.Views;

namespace TeamSoftware.HackDay_HD
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowProperties props;
        private VSTSClient VSTSClient;
        public delegate void PRLoadedHandler(object sender, PRLoadedEventArgs e);
        public event PRLoadedHandler OnPRLoaded;
        ObjectCache memoryCache;

        public MainWindow()
        {
            InitializeComponent();
            props = new MainWindowProperties();
            this.DataContext = props;
            this.VSTSClient = new VSTSClient();
            memoryCache = MemoryCache.Default;

            this.OnPRLoaded += MainWindow_OnPRLoaded;
        }

        private void MainWindow_OnPRLoaded(object sender, PRLoadedEventArgs e)
        {
            Task.Run(() =>
            {
                Parallel.ForEach(e.PRs, (pr) =>
                {
                    SetPRWorkItems(pr);
                });
            });
        }

        private bool Filter(PullRequest pr)
        {
            if (chkActive.IsChecked.Value && chkCompleted.IsChecked.Value)
            {
                return pr.Status == PRStatus.Active || pr.Status == PRStatus.Completed;
            }
            else if (chkCompleted.IsChecked.Value)
            {
                return pr.Status == PRStatus.Completed;
            }
            else if (chkActive.IsChecked.Value)
            {
                return pr.Status == PRStatus.Active;
            }


            return false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var projects = VSTSClient.GetProjects().ToList();
            props.Projects = projects.OrderBy(p => p.Name).ToList();
            var projectIds = projects.Select(x => x.ID);

            foreach (var projectId in projectIds)
            {
                var repos = VSTSClient.GetProjectRepositories(projectId.ToString()).OrderBy(x => x.Name.ToUpper()).ToList();
                PreselectRepos(repos);
                props.Repositories.AddRange(repos);
            }
            UpdatePRs();
        }

        private void PreselectRepos(List<Models.Repository> repos)
        {
            string repoList = Properties.Settings.Default["SavedRepos"].ToString();

            foreach (Models.Repository repo in repos)
            {
                if (repoList.Contains(repo.ID.ToString()))
                {
                    repo.IsChecked = true;
                }
            }
        }

        private void UpdateRepos(object sender)
        {
            var projectList = sender as ListView;
            if (projectList != null)
            {
                var project = projectList.SelectedItem as Project;
                if (project != null)
                {
                    foreach (var repo in props.Repositories)
                    {
                        if (repo.Project.Name == project.Name)
                        {
                            repo.IsVisible = true;
                        }
                        else
                        {
                            repo.IsVisible = false;
                        }
                    }

                    if (props.Repositories.Count() == 0)
                    {
                        repoControlList.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        repoControlList.Visibility = Visibility.Visible;
                    }
                }
            }
        }

        private void click_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            UpdateRepos(sender);
        }

        private void click_KeyUp(object sender, KeyEventArgs e)
        {
            UpdateRepos(sender);
        }

        private void UpdatePRs()
        {
            props.PRList.Clear();
            var checkedRepos = props.Repositories.Where(r => r.IsChecked).ToList();
            var prs = new List<PullRequest>();
            foreach (var repo in checkedRepos)
            {
                prs.AddRange(VSTSClient.GetPullRequests(repo.Project.ID.ToString(), repo.Name));
            }

            props.PRList = new ObservableCollection<PullRequest>(prs);

            CollectionViewSource.GetDefaultView(listViewPR.ItemsSource).Filter = pr => Filter((PullRequest)pr);
            PRLoadedEventArgs args = new PRLoadedEventArgs() { PRs = prs };

            OnPRLoaded(this, args);
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var repo = ((sender as CheckBox).DataContext as Models.Repository);
          //  repo.IsChecked = !repo.IsChecked;
            AddOrRemovedSavedRepos(repo);
            UpdatePRs();
        }

        private void AddOrRemovedSavedRepos(Models.Repository repo)
        {
            if (repo.IsChecked)
            {
                Properties.Settings.Default["SavedRepos"] += repo.ID + " ";
            }
            else
            {
                string repoList = Properties.Settings.Default["SavedRepos"].ToString();
                Properties.Settings.Default["SavedRepos"] = repoList.Replace(repo.ID + " ", "");
            }
            Properties.Settings.Default.Save();

        }

        private void listViewPR_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var PR = ((sender as ListView).SelectedItem as PullRequest);
            if (PR != null)
            {
                Process.Start(PR.URL);
            }
        }

        private void listViewPR_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var PR = ((sender as ListView).SelectedItem as PullRequest);
            if (PR != null)
            {
                props.WIList.Clear();
                var workItemIDs = VSTSClient.GetWorkItemsFromPR(PR);
                var workItems = VSTSClient.GetWorkItems(workItemIDs);
                PR.WorkItems = workItems;
                PR.ReleaseIndicator = string.Join(", ", workItems.Select(x => x.ReleaseIndicator).Distinct());
                props.WIList = new ObservableCollection<WorkItem>(PR.WorkItems);

                lvReviewers.ItemsSource = PR.GetListOfReviewers();
            }
        }

        private void listViewWI_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var WI = ((sender as ListView).SelectedItem as WorkItem);
            if (WI != null)
            {
                Process.Start(WI.URL);
            }
        }

        private void SetPRWorkItems(PullRequest pr)
        {
            var cacheKey = $"PR_WI_{pr.ID}";
            bool needsUpdate = true;
            var workItemIDs = new List<string>();
            var workItems = new List<WorkItem>();
            if (memoryCache.Contains(cacheKey))
            {
                var WI = memoryCache[cacheKey] as List<WorkItem>;
                if (WI != null)
                {
                    workItems = WI;
                    needsUpdate = false;
                }
            }

            if (needsUpdate)
            {
                workItemIDs = VSTSClient.GetWorkItemsFromPR(pr);
                workItems = VSTSClient.GetWorkItems(workItemIDs);

                var cacheItem = new CacheItem(cacheKey, workItems);
                var cachePolicy = new CacheItemPolicy() { AbsoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(3) };
                memoryCache.Add(cacheItem, cachePolicy);
            }

            pr.WorkItems = workItems;
            pr.AreaPath = workItems.Select(x => x.AreaPath).FirstOrDefault();
            pr.ReleaseIndicator = string.Join(", ", workItems.Select(x => x.ReleaseIndicator).Where(x => !string.IsNullOrEmpty(x)).Distinct());
            pr.Patch = workItems.Select(x => x.Patch).FirstOrDefault();
        }

        private void RefreshUI(object sender, EventArgs e)
        {
            if (listViewPR?.ItemsSource != null)
            {
                CollectionViewSource.GetDefaultView(listViewPR.ItemsSource).Refresh();
            }
        }

        //   BindingListCollectionView blcv;
        GridViewColumnHeader _lastHeaderClicked = null;
        ListSortDirection _lastDirection = ListSortDirection.Ascending;

        // Header click event
        void results_Click(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader headerClicked =
            e.OriginalSource as GridViewColumnHeader;
            ListSortDirection direction;

            if (headerClicked != null)
            {
                if (headerClicked.Role != GridViewColumnHeaderRole.Padding)
                {
                    if (headerClicked != _lastHeaderClicked)
                    {
                        direction = ListSortDirection.Ascending;
                    }
                    else
                    {
                        if (_lastDirection == ListSortDirection.Ascending)
                        {
                            direction = ListSortDirection.Descending;
                        }
                        else
                        {
                            direction = ListSortDirection.Ascending;
                        }
                    }

                    //TODO: Change it to use the binding name instead of the header name to find the correct sort
                    string header = headerClicked.Column.Header as string;
                    if (header.Contains("Repository"))
                    {
                        header += ".Name";
                    }
                    Sort(header, direction);

                    if (direction == ListSortDirection.Ascending)
                    {
                        headerClicked.Column.HeaderTemplate =
                          Resources["HeaderTemplateArrowUp"] as DataTemplate;
                    }
                    else
                    {
                        headerClicked.Column.HeaderTemplate =
                          Resources["HeaderTemplateArrowDown"] as DataTemplate;
                    }

                    // Remove arrow from previously sorted header
                    if (_lastHeaderClicked != null && _lastHeaderClicked != headerClicked)
                    {
                        _lastHeaderClicked.Column.HeaderTemplate = null;
                    }

                    _lastHeaderClicked = headerClicked;
                    _lastDirection = direction;
                }
            }
        }

        // Sort code
        private void Sort(string sortBy, ListSortDirection direction)
        {
            ICollectionView dataView =
           CollectionViewSource.GetDefaultView(listViewPR.ItemsSource);

            dataView.SortDescriptions.Clear();
            SortDescription sd = new SortDescription(sortBy, direction);
            dataView.SortDescriptions.Add(sd);
            dataView.Refresh();
        }

    }
}
