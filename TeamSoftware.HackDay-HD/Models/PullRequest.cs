﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamSoftware.HackDay_HD.Views;

namespace TeamSoftware.HackDay_HD.Models
{
    [DisplayName("Created By")]
    public class CreatedBy
    {
        public Guid ID { get; set; }
        public string DisplayName { get; set; }

        public override string ToString()
        {
            return DisplayName;
        }
    }

    public class Reviewer
    {
        public string DisplayName { get; set; }
        public PRVote Vote { get; set; }
        public string URL { get; set; }
    }


    public enum PRStatus
    {
        Active,
        Abandoned,
        Completed
    }

    public enum PRVote
    {
        Rejected = -10,
        Approved = 10,
        ApprovedWithSuggestions = 5
    }

    public class PullRequest : ModelBase
    {
        [JsonProperty("PullRequestID")]
        public string ID { get; set; }
        public Repository Repository { get; set; }
        public PRStatus Status { get; set; }
        [DisplayName("Created By")]
        public CreatedBy CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public string Title { get; set; }

        [JsonIgnore]
        public string URL
        {
            get
            {
                return $"https://teamsoftwareinc.visualstudio.com/{this.Repository.Project.Name}/_git/{this.Repository.Name}/pullrequest/{this.ID}?_a=overview";
            }
        }

        public string Description { get; set; }

        public List<WorkItem> WorkItems { get; set; }

        [JsonIgnore]
        private string _releaseIndicator;
        [JsonIgnore]
        public string ReleaseIndicator
        {
            get
            {
                return _releaseIndicator;
            }

            set
            {
                _releaseIndicator = value;
                OnPropertyChanged("ReleaseIndicator");
            }
        }

        [JsonIgnore]
        private string _patch;
        [JsonIgnore]
        public string Patch
        {
            get
            {
                return _patch;
            }

            set
            {
                _patch = value;
                OnPropertyChanged("Patch");
            }
        }

        [JsonIgnore]
        private string _reviewersToolTip;
        [JsonIgnore]
        public string ReviewersToolTip
        {
            get
            {
                return _reviewersToolTip;
            }

            set
            {
                _reviewersToolTip = value;
                OnPropertyChanged("ReviewersToolTip");
            }
        }

        [JsonIgnore]
        private string _areaPath;
        [JsonIgnore]
        public string AreaPath
        {
            get
            {
                return _areaPath;
            }

            set
            {
                _areaPath = value;
                OnPropertyChanged("AreaPath");
            }
        }

        private List<Reviewer> _reviwers;
        public List<Reviewer> Reviewers
        {
            get
            {
                return _reviwers;
            }

            set
            {
                _reviwers = value;
                _reviewersToolTip = GetListOfReviewersString();
            }
        }

       
        public List<Reviewer> GetListOfReviewers()
        {
          return _reviwers.Where(r => !r.DisplayName.ToLower().Contains("winteam") && !r.DisplayName.ToLower().ToString().Contains("ehub mobile")).ToList();
        }
        public string GetListOfReviewersString()
        {
            List<Reviewer> reviwers = _reviwers.Where(r => !r.DisplayName.ToLower().Contains("winteam") && !r.DisplayName.ToLower().ToString().Contains("ehub mobile")).ToList();
            string returnstring = "";

            for(int i=0; i <= reviwers.Count - 1; i++)
            {
                Reviewer reviwer = reviwers[i];
                returnstring += reviwer.DisplayName + " " + reviwer.Vote.ToString() + (i == reviwers.Count -1 ? "" : Environment.NewLine);
            }

            return returnstring;
        }
    }
}
