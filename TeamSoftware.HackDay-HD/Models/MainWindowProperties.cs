﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamSoftware.HackDay_HD.Views;

namespace TeamSoftware.HackDay_HD.Models
{
    public class MainWindowProperties : ModelBase
    {
        private List<Project> _projects = new List<Project>();
        public List<Project> Projects
        {
            get
            {
                return _projects;
            }
            set
            {
                _projects = value;
                OnPropertyChanged("Projects");
            }
        }

        private List<Repository> _repositories = new List<Repository>();
        public List<Repository> Repositories
        {
            get
            {
                return _repositories;
            }
            set
            {
                _repositories = value;
                OnPropertyChanged("Repositories");
            }
        }

        private ObservableCollection<PullRequest> _prList = new ObservableCollection<PullRequest>();
        public ObservableCollection<PullRequest> PRList
        {
            get
            {
                return _prList;
            }
            set
            {
                _prList = value;
                OnPropertyChanged("PRList");
            }
        }

        private ObservableCollection<WorkItem> _wiList = new ObservableCollection<WorkItem>();
        public ObservableCollection<WorkItem> WIList
        {
            get
            {
                return _wiList;
            }
            set
            {
                _wiList = value;
                OnPropertyChanged("WIList");
            }
        }
    }
}
