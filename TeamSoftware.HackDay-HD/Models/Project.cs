﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TeamSoftware.HackDay_HD.Views;

namespace TeamSoftware.HackDay_HD.Models
{
    public class Project : ModelBase
    {
        public Guid ID { get; set; }
        [JsonIgnore]
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }
        public string Description { get; set; }
        public string URL { get; set; }
        public string State { get; set; }// enum?
        public int Revision { get; set; }
        public string Visibility { get; set; } //enum?

        [JsonIgnore]
        private bool _isSelected;
        [JsonIgnore]
        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }

            set
            {
                _isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }
    }
}
