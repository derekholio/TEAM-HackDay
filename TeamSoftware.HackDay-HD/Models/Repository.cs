﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamSoftware.HackDay_HD.Views;

namespace TeamSoftware.HackDay_HD.Models
{
    public class Repository : ModelBase
    {
        public Guid ID { get; set; }

        [JsonIgnore]
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        public string URL { get; set; }
        public Project Project { get; set; }
        public string DefaultBranch { get; set; }
        public string RemoteURL { get; set; }

        [JsonIgnore]
        private bool _isChecked;
        [JsonIgnore]
        public bool IsChecked
        {
            get
            {
                return _isChecked;
            }

            set
            {
                _isChecked = value;
                OnPropertyChanged("IsChecked");
            }
        }

        [JsonIgnore]
        private bool _isVisible;
        [JsonIgnore]
        public bool IsVisible
        {
            get
            {
                return _isVisible;
            }

            set
            {
                _isVisible = value;
                OnPropertyChanged("IsVisible");
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
