﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamSoftware.HackDay_HD.Models
{
    public class WorkItem
    {
        public string ID { get; set; }
        [JsonProperty("System.Title")]
        public string Title { get; set; }

        [JsonProperty("System.State")]
        public string State { get; set; }

        [JsonProperty("System.WorkItemType")]
        public string WorkItemType { get; set; }

        [JsonProperty("TEAMAgile.ReleaseIndicator")]
        public string ReleaseIndicator { get; set; }

        [JsonProperty("System.AreaPath")]
        public string AreaPath { get; set; }
        [JsonProperty("TEAMAgile.ContentStatus")]
        public string ContentStatus { get; set; }

        [JsonProperty("TEAMAgile.Patch")]
        public string Patch { get; set; }

        [JsonProperty("Microsoft.VSTS.Common.StateChangeDate")]
        public DateTime StateChangeDate { get; set; }

        [JsonIgnore]
        public string URL { get; set; }
    }
}
