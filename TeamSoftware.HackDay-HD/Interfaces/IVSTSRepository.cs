﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamSoftware.HackDay_HD.Models;

namespace TeamSoftware.HackDay_HD.Interfaces
{
    public interface IVSTSClient
    {
        List<Project> GetProjects();

        List<Models.Repository> GetRepositories();

        List<Models.Repository> GetProjectRepositories(string projectId);

        List<PullRequest> GetPullRequests(string projectId, string repoName);

        List<WorkItem> GetWorkItems(List<string> workItemIDs);

        List<string> GetWorkItemsFromPR(PullRequest pr);
    }
}
