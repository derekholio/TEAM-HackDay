﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamSoftware.HackDay_HD.Models;

namespace TeamSoftware.HackDay_HD.Events
{
    public class PRLoadedEventArgs : EventArgs
    {
        public List<PullRequest> PRs { get; set; }
    }
}
