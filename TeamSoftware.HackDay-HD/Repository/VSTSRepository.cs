﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamSoftware.HackDay_HD.Interfaces;
using TeamSoftware.HackDay_HD.Models;
using RestSharp;
using RestSharp.Authenticators;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TeamSoftware.HackDay_HD.Repository
{
    public class VSTSClient : IVSTSClient
    {
        private RestClient GetRestClient()
        {
            var client = new RestClient("https://teamsoftwareinc.visualstudio.com");

            return client;
        }

        private RestRequest GetRestRequest(string URL, Method method)
        {
            var request = new RestRequest(URL, method);
            request.AddParameter("api-version", "1.0");
            request.AddHeader("Authorization", "Basic OmN2aWJkbDVoYTV6cjZ1N3d5enVqZnR6Y2ptdHV1YTM0eXM0N3BoMm40dWplZzNvZHd0NXE=");

            return request;
        }

        public List<Project> GetProjects()
        {
            var client = GetRestClient();
            var request = GetRestRequest("defaultcollection/_apis/projects", Method.GET);

            var response = client.Execute(request);
            var json = response.Content;
            var values = JObject.Parse(json)["value"];

            var data = JsonConvert.DeserializeObject<List<Project>>(values.ToString());

            return data;
        }

        public List<Models.Repository> GetRepositories()
        {
            var client = GetRestClient();
            var request = GetRestRequest($"DefaultCollection/_apis/git/repositories", Method.GET);

            var response = client.Execute(request);
            var json = response.Content;
            var values = JObject.Parse(json)["value"];

            var data = JsonConvert.DeserializeObject<List<Models.Repository>>(values.ToString());

            return data;
        }

        public List<Models.Repository> GetProjectRepositories(string projectId)
        {
            var client = GetRestClient();
            var request = GetRestRequest($"DefaultCollection/{projectId}/_apis/git/repositories", Method.GET);

            var response = client.Execute(request);
            var json = response.Content;
            var values = JObject.Parse(json)["value"];

            var data = JsonConvert.DeserializeObject<List<Models.Repository>>(values.ToString());

            return data;
        }

        public List<PullRequest> GetPullRequests(string projectId, string repoName)
        {
            var client = GetRestClient();
            var request = GetRestRequest($"/DefaultCollection/{projectId}/_apis/git/repositories/{repoName}/pullRequests?status=all", Method.GET);

            var response = client.Execute(request);
            var json = response.Content;
            var values = JObject.Parse(json)["value"];

            var data = JsonConvert.DeserializeObject<List<PullRequest>>(values.ToString());

            return data;
        }

        public List<WorkItem> GetWorkItems(List<string> workItemIDs)
        {
            if(workItemIDs.Count < 1)
            {
                return new List<WorkItem>();
            }

            var ret = new List<WorkItem>();
            var client = GetRestClient();
            var request = GetRestRequest($"/DefaultCollection/_apis/wit/workitems", Method.GET);
            request.AddParameter("$expand", "all");
            request.AddParameter("ids", string.Join(",", workItemIDs));

            var response = client.Execute(request);
            var json = response.Content;
            var values = JObject.Parse(json)["value"];

            foreach(dynamic workItem in values)
            {
                var workItemId = workItem["id"].ToString();
                if (workItemId == "41355")
                {
                    var a = "";
                }
                var fields = workItem["fields"];
                var newWorkItem = JsonConvert.DeserializeObject<WorkItem>(fields.ToString());
                newWorkItem.ID = workItemId;

                var link = workItem["_links"]["html"]["href"].ToString();
                newWorkItem.URL = link;

                ret.Add(newWorkItem);
            }

            return ret;
        }

        public List<string> GetWorkItemsFromPR(PullRequest pr)
        {
            var ret = new List<String>();
            var client = GetRestClient();
            var request = GetRestRequest($"/DefaultCollection/{pr.Repository.Project.Name}/_apis/git/repositories/{pr.Repository.Name}/pullRequests/{pr.ID}/workitems", Method.GET);

            var response = client.Execute(request);
            var json = response.Content;
            var values = JObject.Parse(json)["value"];
            var valuesArray = JArray.Parse(values.ToString());

            foreach(dynamic value in values)
            {
                ret.Add(value["id"].ToString());
            }

            return ret;
        }
    }
}
